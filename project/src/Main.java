import java.util.HashMap;

public class Main {

	private static BeepService beepService = new BeepService();
	private static RomanService romanService = new RomanService();

	private static HashMap<Character, Integer> toneMap = new HashMap<Character, Integer>();
	
	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		// 37, 39, 42, 44, 46,
		// 49, 51, 54, 56, 58,
		// 61, 63, 66, 68, 70, //normal
		// 73, 75, 78, 80, 82,
		// 85, 87, 90, 92, 94,
		// 97, 99,102,104,106,

		toneMap.put('ↂ', 102);// 10000
		toneMap.put('ↁ', 97); // 5000 =
		toneMap.put('M', 90); // 1000
		toneMap.put('D', 85); //  500 =
		toneMap.put('C', 78); //  100
		toneMap.put('L', 73); //   50 =
		toneMap.put('X', 66); //   10
		toneMap.put('V', 61); //    5 =
		toneMap.put('I', 54); //    1
		toneMap.put(' ',  0); //    -
		
		beepService.setWaitTime(200);

		beepNumber(6,  false, true); trySleep(1000);
		beepNumber(9,  false, true); trySleep(1000);
		beepNumber(19, false, true); trySleep(1000);
		beepNumber(40, false, true); trySleep(1000);
		beepNumber(44, false, true); trySleep(1000);
		beepNumber(49, false, true); trySleep(1000);

		beepService.dieWhenDone();
	}

	private static void beepNumber(int nr, boolean useSubtractRule, boolean makePauses) {
		String roman = romanService.toRoman(nr, useSubtractRule, makePauses);
		System.out.println(roman);

		beepString(roman);
	}
	
	private static void beepString(String str) {
		for (char c : str.toCharArray()) {
			Integer tone = toneMap.get(c);
			if (tone != null) {
				beepService.beep(tone);
			} else {
				System.err.println("No tone found for '" + c + "'!");
			}
		}
		beepService.waitFor();
	}
	
	private static void trySleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}
}
