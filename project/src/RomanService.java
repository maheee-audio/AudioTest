import java.util.Arrays;

public class RomanService {

	//                                              0     1     2     3     4     5     6     7     8
	private final int[]  values  = new int[]  { 10000, 5000, 1000,  500,  100,   50,   10,    5,    1 };
	private final char[] symbols = new char[] {   'ↂ',  'ↁ',  'M',  'D',  'C',  'L',  'X',  'V',  'I' };
	private final int[]  subtr   = new int[]  {    -1,   -1,    4,    4,    6,    6,    8,    8,   -1 };

	public int getValue(int nr) {
		if (nr >= 0 && nr < values.length) {
			return values[nr];
		}
		return -1;
	}

	public char getSymbol(int nr) {
		if (nr >= 0 && nr < symbols.length) {
			return symbols[nr];
		}
		return 0;
	}

	private String getSymbols(int i, int count) {
		char[] res = new char[count];
		Arrays.fill(res, getSymbol(i));
		return new String(res);
	}

	public String toRoman(int number) {
		return toRoman(number, true, false);
	}

	public String toRoman(int number, boolean useSubtrRule, boolean addSpaces) {
		if (number < 0) {
			return null;
		}

		StringBuilder res = new StringBuilder();

		for (int i = 0; i < values.length; ++i) {
			int amount, value;

			value = values[i];
			amount = number / value;
			number %= value;

			res.append(getSymbols(i, amount));
			if (addSpaces && amount > 0) {
				res.append(' ');
			}

			if (useSubtrRule && subtr[i] >= 0) {
				int subtrPos = subtr[i];
				value = values[i] - values[subtrPos];
				amount = number / value;
				
				if (amount == 1) {
					number %= value;
	
					res.append(getSymbol(subtrPos));
					res.append(getSymbol(i));
					
					if (addSpaces) {
						res.append(' ');
					}
				}
			}
		}

		return res.toString();
	}

}
