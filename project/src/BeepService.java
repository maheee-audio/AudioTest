import java.util.LinkedList;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

public class BeepService {

	private Receiver receiver;

	/*private int[] notes = new int[] {
			49, 51, 54, 56, 58,
			61, 63, 66, 68, 70, //normal
			73, 75, 78, 80, 82,
			85, 87, 90, 92, 94
			//60, 62, 64, 65, 67, 69, 71, //normal
			//72, 74, 76, 77, 79, 81, 83,
			//84, 86, 88, 89, 91, 93, 95
			};*/

	private LinkedList<MidiMessage> messageQueue = new LinkedList<MidiMessage>();

	private volatile boolean killThread = false;

	private int waitTime = 0;

	private Runnable beepThread = new Runnable() {

		@Override
		public void run() {
			MidiMessage msg = null;
			while (!killThread) {
				synchronized (messageQueue) {
					msg = messageQueue.isEmpty() ?
							null : messageQueue.removeFirst();
				}
				if (msg != null) {
					receiver.send(msg, -1);
					trySleep(waitTime);
				} else {
					trySleep(100);
				}
			}
		}
	};
	
	private void trySleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

	public BeepService() {
		try {
			receiver = MidiSystem.getReceiver();
		} catch (MidiUnavailableException e) {
			throw new RuntimeException(e);
		}
		(new Thread(beepThread)).start();
	}

	public BeepService beep(int tone) {
		synchronized (messageQueue) {
			messageQueue.addLast(createBeepMessage(/*notes[tone]*/tone));
		}
		return this;
	}

	private MidiMessage createBeepMessage(int note) {
		ShortMessage msg = new ShortMessage();
		try {
			msg.setMessage(ShortMessage.NOTE_ON, 0, note, 93);
		} catch (InvalidMidiDataException e) {
			System.err.println("Can't play tone '" + note + "'!");
		}
		return msg;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public BeepService setWaitTime(int waitTime) {
		this.waitTime = waitTime;
		return this;
	}

	public void die() {
		killThread = true;
	}

	public void dieWhenDone() {
		waitFor();
		die();
	}

	public BeepService waitFor() {
		while (true) {
			boolean done;
			synchronized (messageQueue) {
				done = messageQueue.isEmpty();
			}
			try {
				if (done) {
					Thread.sleep(waitTime);
					return this;
				} else {
					Thread.sleep(100);
				}
			} catch (InterruptedException e) {
			}
		}
	}
}
